# README #

This README would normally document whatever steps are necessary to get your application up and running.

### ¿Para qué es el repositorio? ###

El repositorio contiene una API cuya función es obtener y procesar noticias de una única fuente Atom.

### ¿Cómo se usa? ###

Primero es necesario crear el objeto por el cual se accede a la API.

"MainModelAPI api = MainModelAPI.getInstance();"

Para añadir urls Atom se utiliza el siguiente metodo: addURL(String url)

"api.addURL("http://www.theregister.co.uk/science/headlines.atom");"

Para acceder y utilizar el parser de noticias primero se crea el objeto NewsModelAPI

"NewsModelAPI nma = api.getNewsModelAPI();"

El metodo getNews() sobre el objeto NewsModelAPI retornará una lista del objeto APIFeed, que son las noticias recuperadas de cada url introducido con el metodo addURL().

"List<APIFeed> news = nma.getNews();"


### Casos excepcionales ###

La API lanzará una excepcion InvalidURLException si alguno de los urls ingresados con el metodo addURL() es incorrecto o inválido.