package apiNews.logic;

/**
 * Clase que actua como punto de entrada a la API
 * 
 * @author Constanza Giorgetti - Franco Peratta Arquitectura y Dise�o de
 *         Sistemas. Proyecto 2017
 */
public class MainModelAPI {
	private static MainModelAPI api = null;
	private URLHandlerI url = new URLHandler();

	public static MainModelAPI getInstance() {
		if (api == null) {
			api = new MainModelAPI();
		}
		return api;
	}

	public NewsModelAPI getNewsModelAPI() {
		return new NewsAtomAPI(url);
	}

	public void addURL(String url) {
		this.url.addURL(url);
	}

}