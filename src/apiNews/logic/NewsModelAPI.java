package apiNews.logic;

import java.util.List;
import apiNews.model.APIFeed;
import apiNews.model.InvalidURLException;

/**
 * Interfaz publica del modelo de noticias
 * 
 * @author Constanza Giorgetti - Franco Peratta Proyecto Arquitectura y Dise�o
 *         de Sistemas 2017
 */
public interface NewsModelAPI {

	/**
	 * Metodo para obtener las noticias
	 * 
	 * @return arreglo con elementos de tipo Feed correspondientes a las
	 *         noticias
	 */
	public List<APIFeed> getNews() throws InvalidURLException;

}
