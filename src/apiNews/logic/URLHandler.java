package apiNews.logic;

import java.util.ArrayList;
import java.util.List;
/**
 * Implementación concreta del URLHandlerI
 * @author Constanza Giorgetti - Franco Peratta
 * Arquitectura y Diseño de Sistemas. Proyecto 2017
 */
class URLHandler implements URLHandlerI {
	List<String> urls;

	public URLHandler() {
		urls = new ArrayList<>();
	}

	public void addURL(String url) {
		urls.add(url);
	}

	public List<String> getURLS() {
		return urls;
	}
}