package apiNews.logic;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import apiNews.model.APIFeed;
import apiNews.model.InvalidURLException;

/**
 * Implementacion concreta del NewsModelAPI
 * 
 * @author Constanza Giorgetti - Franco Peratta Proyecto Arquitectura y Dise�o
 *         de Sistemas 2017
 */
class NewsAtomAPI implements NewsModelAPI {

	private URLHandlerI handler;

	public NewsAtomAPI(URLHandlerI url) {
		handler = url;
	}

	@Override
	public List<APIFeed> getNews() throws InvalidURLException {

		List<APIFeed> feed = new ArrayList<APIFeed>();
		List<String> urls = handler.getURLS();

		for (int x = 0; x < urls.size(); x++) {

			try {
				URL url = new URL(urls.get(x));
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = null;

				db = dbf.newDocumentBuilder();

				Document doc = db.parse(url.openStream());
				NodeList items = doc.getDocumentElement().getElementsByTagName("entry");

				for (int i = 0; i < items.getLength(); i++) {

					Element item = (Element) items.item(i);
					Element title = (Element) item.getElementsByTagName("title").item(0);
					Element link = (Element) item.getElementsByTagName("link").item(0);

					feed.add(new APIFeed(title.getTextContent(), link.getAttribute("href")));

				}

			} catch (SAXException e) {
				// handle SAXException
			} catch (IOException e) {
				// handle IOException
				throw new InvalidURLException("Invalid url");
			} catch (ParserConfigurationException e1) {
				// handle ParserConfigurationException
			}
			// VER CUAL ES LA EXCEPCION PARA URL INCORRECTO.
		}
		return feed;
	}

}