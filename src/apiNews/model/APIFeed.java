package apiNews.model;
/**
 * Abstraccion del objeto de dato noticia
 * @author Constanza Giorgetti - Franco Peratta
 * Proyecto Arquitectura y Dise�o de Sistemas 2017
 */
public class APIFeed {

	private String title;
	private String link;
	
	public APIFeed(String t, String l){
		title=t;
		link=l;
	}
	
	public void setTitle(String t){
		title=t;
	}
	
	public void setLink(String l){
		link=l;
		
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getLink(){
		return link;
	}
}
