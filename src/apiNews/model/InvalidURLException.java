package apiNews.model;

public class InvalidURLException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidURLException() {
		super();
	}

	public InvalidURLException(String message) {
		super(message);
	}
}